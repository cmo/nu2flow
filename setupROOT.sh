#!/bin/sh

echo "************************************************************************"
echo -e "SETUP\t Setting up ATLAS software and ROOT"
echo ""

shopt -s expand_aliases
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# Call setupATLAS.
echo "************************************************************************"
echo -e "SETUP\t Executing \"setupATLAS --quiet\""
setupATLAS --quiet || { echo -e "\"setupATLAS\" failed. Aborting ..."; return 1; }
echo ""

# set up root because why not.
echo "************************************************************************"
echo -e "SETUP\t Executing \"lsetup \"root 6.28.08-x86_64-centos7-gcc11-opt\" \""
lsetup "root 6.28.08-x86_64-centos7-gcc11-opt" || { echo -e "\"lsetup \"root 6.28.08-x86_64-centos7-gcc11-opt\" \" failed. Aborting ..."; return 1; }
echo ""

echo "************************************************************************"
echo -e "SETUP\t Configuration finished!"
echo -e "SETUP\t "

