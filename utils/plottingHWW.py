"""A collection of plotting scripts for standard uses."""

from copy import deepcopy
from functools import partial
from pathlib import Path
from typing import Callable, Optional, Union

import matplotlib
import matplotlib.axes._axes as axes
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.path as mpth
from matplotlib import markers
import numpy as np
import pandas as pd
import PIL.Image
import seaborn as sns
from matplotlib.colors import LinearSegmentedColormap, LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.interpolate import make_interp_spline
from scipy.stats import binned_statistic, pearsonr
from typing import Union
import h5py

# Some defaults for my plots to make them look nicer
plt.rcParams["xaxis.labellocation"] = "right"
plt.rcParams["yaxis.labellocation"] = "top"
plt.rcParams["legend.edgecolor"] = "1"
plt.rcParams["legend.loc"] = "upper left"
plt.rcParams["legend.framealpha"] = 0.0
plt.rcParams["axes.labelsize"] = "large"
plt.rcParams["axes.titlesize"] = "large"
plt.rcParams["legend.fontsize"] = 11

def plot_event_prediction_hist(
        flows: Union[list, np.ndarray],
        truth: Union[list, np.ndarray],
        flows_labels: Union[list, str],
        truth_labels: Union[list, str],
        x_label: str,
        start: int = 0,
        end: int = 10,
        flows_kwargs: Optional[list] = None,
        truth_kwargs: Optional[list] = None,
        y_label: Optional[str] = None,
        path: Optional[Union[Path, str]] = None,
        bins: Union[list, str, partial] = "auto",
        incl_overflow: bool = True,
        incl_underflow: bool = True,
        incl_flows_pred: bool = False,
        do_norm: bool = True,
        scale_factors: Optional[list] = None,
        logy: bool = False,
        ylim: Optional[float] = None,
        ypad: float = 1.5,
        scale: int = 5,
        do_legend: bool = True,
        legend_kwargs: Optional[list] = None,
        return_fig: bool = False,
        return_img: bool = False,
) -> Union[plt.Figure, None]:

    """Plot sampled PDF neutrino kinematics for individual events vs truth values.
    - Performs the histogramming in place
    - Input flow and truth data seperately
    - Providing a list of nu,anti_nu per event will plot both on same axis

    args:
        flows: A list of tensors or numpy arrays, for one or both flows neutrinos, each col will be superimposed on the same axis
        truth: A list of tensors or numpy arrays, for one or both turth neutrinos, each col will be superimposed on the same axis
        flows_labels: A list of labels for each tensor in flows
        truth_labels: A list of labels for each tensor in truth
        flows_kwargs: Additional keyword arguments for the flows line(s) for each histogram
        truth_kwargs: Additional keyword arguments for the truth line(s) for each histogram
        start: Index of first event to plot
        end: Index of last event to plot
        x_label: Label for the x-axis
        y_label: Optional label for the y-axis
        path: The save location of the plots (include img type)
        bins: List of bins to use for each axis, can use numpy's strings
        incl_overflow: Have the final bin include the overflow
        incl_underflow: Have the first bin include the underflow
        incl_flows_pred: Include lines for chosen flows prediction (first sample)
        do_norm: normalises plot over number of samples drawn from pdf (default is 255)
        scale_factors: Optional list of scalars to be applied to each histogram, goes [flows,truth]
        logy: If we should use the log in the y-axis
        ylim: The y limits for all plots
        ypad: The amount by which to pad the whitespace above the plots
        scale: The size in inches for each subplot
        do_legend: If the legend should be plotted
        legend_kwargs: Extra keyword arguments to pass to the legend constructor
        return_fig: Return the figure (DOES NOT CLOSE IT!)
        return_img: Return a PIL image (will close the figure)
    """

    # Make the arguments lists for generality
    if not isinstance(flows, list):
        flows = [flows]
    if not isinstance(truth, list):
        truth = [truth]
    if isinstance(flows_labels, str):
        flows_labels = [flows_labels]
    if isinstance(truth_labels, str):
        truth_labels = [truth_labels]
    if not isinstance(scale_factors, list):
        scale_factors = (len(flows) + len(truth)) * [scale_factors]

    if (len(flows) > 1):
        print("Plotting neutrino and anti_neutrino on same axis")

    #Make sure each input is at-least 2D (event,sample)
    #Make copies in case we add more hist in the future
    data_list = flows
    data_labels = flows_labels
    hist_kwargs = flows_kwargs
    for ID in range(len(data_list)):
        if data_list[ID].ndim < 2:
            data_list[ID] = data_list[ID].unsqueeze(-1)

    # Check the number of histograms to plot for each event
    n_data = len(data_list)
    n_axis = 1

    # Make sure that all the list lengths are consistant
    assert len(data_labels) == n_data

    # Determine the bins that should be used
    # Automatic/Integer bins are replaced using the flows values from all events
    if isinstance(bins, partial):
        bins = bins()

    # If the bins was specified to be 'auto' or another numpy string
    if isinstance(bins, str):
        unq = np.unique(data_list[0][:])
        n_unique = len(unq)

        # If the number of datapoints is less than 10 then use even spacing
        if 1 < n_unique < 10:
            bins = (unq[1:] + unq[:-1]) / 2  # Use midpoints, add final, initial
            bins = np.append(bins, unq.max() + unq.max() - bins[-1])
            bins = np.insert(bins, 0, unq.min() + unq.min() - bins[0])

        elif bins == "quant":
            bins = quantile_bins(data_list[0][:])

    # Numpy function to get the bin edges, catches all other cases (int, etc)
    bins = np.histogram_bin_edges(data_list[0][:], bins=bins)


    #Loop over the event range
    for evt in range(start,end):

        # Create the figure and axes lists
        dims = np.array([1, n_axis])  # Subplot is (n_rows, n_columns)
        size = np.array([n_axis, 1.0])  # Size is (width, height)
        fig, axes = plt.subplots(
            *dims,
            figsize=tuple(scale * size),
            squeeze=False,
        )

        # Cycle through each of the data arrays
        for data_idx in range(n_data):
            # Apply overflow and underflow (make a copy)
            data = np.copy(data_list[data_idx][evt]).squeeze()
            if incl_overflow:
                data = np.minimum(data, bins[-1])
            if incl_underflow:
                data = np.maximum(data, bins[0])

            # If the data is still a 2D tensor treat it as a collection of histograms
            if data.ndim > 1:
                h = []
                for dim in range(data.shape[-1]):
                    h.append(np.histogram(data[:, dim], bins)[0])

                # Nominal and err is based on chi2 of same value, mult measurements
                hist = 1 / np.mean(1 / np.array(h), axis=0)
                hist_err = np.sqrt(1 / np.sum(1 / np.array(h), axis=0))

            # Otherwise just calculate a single histogram
            else:
                hist, _ = np.histogram(data, bins)
                hist_err = np.sqrt(hist)

            # Manually do the density so that the error can be scaled
            if do_norm:
                divisor = np.array(np.diff(bins), float) / hist.sum()
                hist = hist * divisor
                hist_err = hist_err * divisor

            # Apply the scale factors
            if scale_factors[data_idx] is not None:
                hist *= scale_factors
                hist_err *= scale_factors

            # Get the additional keyword arguments for the histograms and errors
            if hist_kwargs[data_idx] is not None and bool(hist_kwargs[data_idx]):
                h_kwargs = deepcopy(hist_kwargs[data_idx])
            else:
                h_kwargs = {}
            # Use the stair function to plot the histograms
            line = axes[0, 0].stairs(
                hist, bins, label=data_labels[data_idx], **h_kwargs
            )

        #Add the truth line(s)
        for truth_idx in range(len(truth)):

            # Get the additional keyword arguments for the histograms and errors
            if truth_kwargs[truth_idx] is not None and bool(truth_kwargs[truth_idx]):
                t_kwargs = deepcopy(truth_kwargs[truth_idx])
            else:
                t_kwargs = {}
            truth_line = axes[0,0].axvline(
                truth[truth_idx][evt], 0, 0.7, label=truth_labels[truth_idx], **t_kwargs
            )

        #Add the [0] flows line
        if incl_flows_pred:
            for flows_idx in range(len(flows)):

                # Get the additional keyword arguments for the histograms and errors
                if flows_kwargs[flows_idx] is not None and bool(flows_kwargs[flows_idx]):
                    f_kwargs = {}
                    if "color" in flows_kwargs[flows_idx]:
                        f_kwargs["color"] = flows_kwargs[flows_idx]["color"]
                    if "linestyle" in flows_kwargs[flows_idx]:
                        f_kwargs["linestyle"] = flows_kwargs[flows_idx]["linestyle"]
                else:
                    f_kwargs = {}
                flows_line = axes[0,0].axvline(
                    flows[flows_idx][evt][0], 0, 0.7, label=flows_labels[flows_idx]+" pred", **f_kwargs
                )


        # Apply editing
        # X axis
        axes[0, 0].set_xlim(bins[0], bins[-1])
        axes[0, 0].set_xlabel(x_label)

        # Y axis
        if logy:
            axes[0, 0].set_yscale("log")
        if ylim is not None:
            axes[0, 0].set_ylim(*ylim)
        else:
            _, ylim2 = axes[0, 0].get_ylim()
            if logy:
                axes[0, 0].set_ylim(top=10 ** (np.log10(ylim2) * ypad))
            else:
                axes[0, 0].set_ylim(top=ylim2 * ypad)
        if y_label is not None:
            axes[0, 0].set_ylabel(y_label)
        elif do_norm:
            axes[0, 0].set_ylabel("Normalised Entries")
        else:
            axes[0, 0].set_ylabel("Entries")

        # Legend
        if do_legend:
            lk = legend_kwargs or {}
            axes[0, 0].legend(**lk)


        # Final figure layout
        fig.tight_layout()

        # Save the file
        if path is not None:
            save_path = str(path)
            if "png" in save_path:
                save_path = save_path.replace(".png","_"+str(evt)+".png")
            elif "pdf" in save_path:
                save_path = save_path.replace(".pdf","_"+str(evt)+".pdf")
            else:
                save_path = save_path.replace("evt","evt_"+str(evt))

            print("Saving: {}".format(save_path))
            fig.savefig(save_path)

        # Return the plot as a rendered image, or the matplotlib figure, or close
        if return_img:
            print("Only generating plot for 'start' event")
            img = PIL.Image.frombytes(
                "RGB", fig.canvas.get_width_height(), fig.canvas.tostring_rgb()
            )
            plt.close(fig)
            return img
        if return_fig:
            print("Only generating plot for 'start' event")
            return fig
        plt.close(fig)


def plot_logprob(
        log_probs: Union[list, np.ndarray],
        log_probs_labels: Union[list, str],
        x_label: str,
        log_probs_kwargs: Optional[list] = None,
        y_label: Optional[str] = None,
        path: Optional[Union[Path, str]] = None,
        bins: Union[list, str, partial] = "auto",
        incl_overflow: bool = True,
        incl_underflow: bool = True,
        incl_all_samples: bool = False,
        do_mean: bool = False,
        do_mode: bool = False,
        do_std: bool = False,
        do_norm: bool = True,
        scale_factors: Optional[list] = None,
        logy: bool = False,
        ylim: Optional[float] = None,
        ypad: float = 1.5,
        scale: int = 5,
        do_legend: bool = True,
        legend_kwargs: Optional[list] = None,
        return_fig: bool = False,
        return_img: bool = False,
) -> Union[plt.Figure, None]:

    """Plot combined nu+anti_nu log_prob_density for all flows choices (or all samples) for chosen flows outputs.
    - Performs the histogramming in place
    - Input different flows outputs as a single list

    args:
        log_probs: A list of tensors or numpy arrays containing flows output "log_prob" data 
        log_probs_labels: A list of labels for each tensor in log_probs
        log_probs_kwargs: Additional keyword arguments for the lines for each histogram
        x_label: Label for the x-axis
        y_label: Optional label for the y-axis
        path: The save location of the plots (include img type)
        bins: List of bins to use for each axis, can use numpy's strings
        incl_overflow: Have the final bin include the overflow
        incl_underflow: Have the first bin include the underflow
        incl_all_samples: Whether to add just the flows neutrino choice (sample 0) or all samples for each event (default 255)
        do_mean: Whether to use the mean of each set of samples as the chosen neutrino
        do_mode: Whether to use the mode of each set of samples as the chosen neutrino
        do_std: Whether to use standard-devation of each set of samples as the seperation metric
        do_norm: normalises plot over number of samples used 
        scale_factors: Optional list of scalars to be applied to each histogram
        logy: If we should use the log in the y-axis
        ylim: The y limits for all plots
        ypad: The amount by which to pad the whitespace above the plots
        scale: The size in inches for each subplot
        do_legend: If the legend should be plotted
        legend_kwargs: Extra keyword arguments to pass to the legend constructor
        return_fig: Return the figure (DOES NOT CLOSE IT!)
        return_img: Return a PIL image (will close the figure)
    """

    # Make the arguments lists for generality
    if not isinstance(log_probs, list):
        log_probs = [log_probs]
    if isinstance(log_probs_labels, str):
        log_probs_labels = [log_probs_labels]
    if not isinstance(scale_factors, list):
        scale_factors = len(log_probs) * [scale_factors]

    #Make sure each input is 2D (event,sample)
    #Make copies in case we add more hist in the future
    data_list = log_probs
    data_labels = log_probs_labels
    hist_kwargs = log_probs_kwargs
    for ID in range(len(data_list)):
        if data_list[ID].ndim < 2:
            data_list[ID] = data_list[ID].unsqueeze(-1)

    # Check the number of histograms to plot for each event
    n_data = len(data_list)
    n_axis = 1

    # Make sure that all the list lengths are consistant
    assert len(data_labels) == n_data

    # Determine the bins that should be used
    # Automatic/Integer bins are replaced using the log_prob values from all samples for first output in list
    if isinstance(bins, partial):
        bins = bins()

    # If the bins was specified to be 'auto' or another numpy string
    if isinstance(bins, str):
        unq = np.unique(data_list[0][:])
        n_unique = len(unq)

        # If the number of datapoints is less than 10 then use even spacing
        if 1 < n_unique < 10:
            bins = (unq[1:] + unq[:-1]) / 2  # Use midpoints, add final, initial
            bins = np.append(bins, unq.max() + unq.max() - bins[-1])
            bins = np.insert(bins, 0, unq.min() + unq.min() - bins[0])

        elif bins == "quant":
            bins = quantile_bins(data_list[0][:])

    # Numpy function to get the bin edges, catches all other cases (int, etc)
    bins = np.histogram_bin_edges(data_list[0][:], bins=bins)

    # Create the figure and axes lists
    dims = np.array([1, n_axis])  # Subplot is (n_rows, n_columns)
    size = np.array([n_axis, 1.0])  # Size is (width, height)
    fig, axes = plt.subplots(
        *dims,
        figsize=tuple(scale * size),
        squeeze=False,
    )

    # Cycle through each of the data arrays
    for data_idx in range(n_data):
        # Apply overflow and underflow (make a copy)
        if do_mean or incl_all_samples or do_std or do_mode:
            data = np.copy(data_list[data_idx][:]).squeeze() 
        else:
            data = np.copy(data_list[data_idx][:][0]).squeeze() 
        if incl_overflow:
            data = np.minimum(data, bins[-1])
        if incl_underflow:
            data = np.maximum(data, bins[0])

        # If the data is still a 2D tensor treat it as a collection of histograms
        if data.ndim > 1:
            print("Data is multi-dim")
            h = []
            means = []
            modes = []
            std_devs = []
            for dim in range(data.shape[-1]):
                h.append(np.histogram(data[:, dim], bins)[0])
            if do_mean or do_std or do_mode:
                for dim in range(data.shape[0]):
                    means.append(np.mean(data[dim, :]))
                    std_devs.append(np.std(data[dim, :]))
                    modes.append(np.max(data[dim, :]))

            # Sum up all histograms
            if do_mean:
                hist, _ = np.histogram(means, bins)
            elif do_mode:
                hist, _ = np.histogram(modes, bins)
            elif do_std:
                #bins = np.histogram_bin_edges(std_devs, bins=25) #Re-calc for 25 equally spaced bins 
                hist, _ = np.histogram(std_devs, bins)
            else:
                hist = np.sum(h,axis=0)
            hist_err = np.sqrt(hist)

        # Otherwise just calculate a single histogram
        else:
            hist, _ = np.histogram(data, bins)
            hist_err = np.sqrt(hist)

        # Manually do the density so that the error can be scaled
        if do_norm:
            divisor = np.array(np.diff(bins), float) / hist.sum()
            hist = hist * divisor
            hist_err = hist_err * divisor

        # Apply the scale factors
        if scale_factors[data_idx] is not None:
            hist *= scale_factors
            hist_err *= scale_factors

        # Get the additional keyword arguments for the histograms and errors
        if hist_kwargs[data_idx] is not None and bool(hist_kwargs[data_idx]):
            h_kwargs = deepcopy(hist_kwargs[data_idx])
        else:
            h_kwargs = {}
        # Use the stair function to plot the histograms
        line = axes[0, 0].stairs(
            hist, bins, label=data_labels[data_idx], **h_kwargs
        )

    # Apply editing
    # X axis
    axes[0, 0].set_xlim(bins[0], bins[-1])
    axes[0, 0].set_xlabel(x_label)

    # Y axis
    if logy:
        axes[0, 0].set_yscale("log")
    if ylim is not None:
        axes[0, 0].set_ylim(*ylim)
    else:
        _, ylim2 = axes[0, 0].get_ylim()
        if logy:
            axes[0, 0].set_ylim(top=10 ** (np.log10(ylim2) * ypad))
        else:
            axes[0, 0].set_ylim(top=ylim2 * ypad)
    if y_label is not None:
        axes[0, 0].set_ylabel(y_label)
    elif do_norm:
        axes[0, 0].set_ylabel("Normalised Entries")
    else:
        axes[0, 0].set_ylabel("Entries")

    # Legend
    if do_legend:
        lk = legend_kwargs or {}
        axes[0, 0].legend(**lk)

    # Final figure layout
    fig.tight_layout()

    # Save the file
    if path is not None:
        save_path = str(path)
        print("Saving: {}".format(save_path))
        fig.savefig(save_path)

    # Return the plot as a rendered image, or the matplotlib figure, or close
    if return_img:
        img = PIL.Image.frombytes(
            "RGB", fig.canvas.get_width_height(), fig.canvas.tostring_rgb()
        )
        plt.close(fig)
        return img
    if return_fig:
        return fig
    plt.close(fig)


def extract_from_target(
        log_probs: np.ndarray, 
        flows: np.ndarray,
        path: Union[Path, str],
        mode: str = "mode",
        width_range: Optional[list] = [0.0,3.0],
        discard_cut_events: Optional[bool] = False,
) -> None:

    """Extracts events from input based on target probability distribution and writes chosen events to file
    - mode options: "mode", "mean", "width", "mode_and_width"
    - "wdith" discards events outside of width_range (if discard_cut_events=False, just replaces values with -999 in order to mask related truth distribution)

    args:
        log_probs: Numpy array containing flows output "log_prob" data 
        flows: Numpy array containing flows output "gen_nu" data (should match log_prob)
        path: The save location of the output file (include extension)
        mode: Criteria used to pick events 
            "mode" = sample with max target prob, 
            "mean" = sample closest to mean of target prob, 
            "width" = Select only events with target prob width in width_range
            "mode_and_width" = Select events by width and then take the mode of those
        discard_cut_events: Whether to remove events outside of width_range or just fill with -999 to maintain input dimensions
    """

    #Make sure log_prob is the correct dimension
    if log_probs.ndim < 2: 
        log_probs = log_probs.unsqueeze(-1)

    #Make sure valid mode is used
    assert mode in ["mode", "mean", "width", "mode_and_width"], 'Invalid mode provided! options are: "mode", "mean", "width", "mode_and_width"'
    assert flows.ndim == 4, 'Invalid flows input, dimension is not 4!'

    #Set up output file (h5 datasets aren't dynamic, need shape ahead of time)
    old_shape = flows.shape
    new_shape = (old_shape[0],1,old_shape[2],old_shape[3])
    output = h5py.File(path, 'w')
    dset = f'gen_nu'
    output.create_dataset(dset,new_shape,flows.dtype)

    # Loop through the events
    mean_ID = 0
    mode_ID = 0
    width_mask = []

    for evt in range(len(log_probs)):
        samples = log_probs[evt]
        mean_ID = np.argmin(abs(samples-np.mean(samples)))
        mode_ID = np.argmax(samples)
        width = np.std(samples)
        pass_evt = width > width_range[0] and width < width_range[1]
        width_mask.append(pass_evt) #Build a mask for cut events

        if mode in ["mode", "mode_and_width"]:
            output[dset][evt,0] = flows[evt][mode_ID]            
        elif mode == "mean":
            output[dset][evt,0] = flows[evt][mean_ID]
        elif mode == "width":
            output[dset][evt,0] = flows[evt][0]
        if mode in ["mode_and_width","width"]:
            if not pass_evt:
                output[dset][evt,0] = -999 

    if discard_cut_events:
        #h5 shape is fixed so we copy data, delete dataset, and remake applying mask
        tmp = np.array(output[dset])[width_mask]
        del output[dset]
        output.create_dataset(dset,tmp)
