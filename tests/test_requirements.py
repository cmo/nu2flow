"""Test availability of required packages."""

import unittest
from pathlib import Path

import pkg_resources

_REQUIREMENTS_PATH = Path("./versions.txt")

class TestRequirements(unittest.TestCase):
    """Test availability of required packages."""

    def test_requirements(self):
        """Test that each required package is available."""
        with open(_REQUIREMENTS_PATH) as f:
            requirements = pkg_resources.parse_requirements(f)
            for requirement in requirements:
                requirement = str(requirement)
                with self.subTest(requirement=requirement):
                    pkg_resources.require(requirement)

if __name__ == '__main__':
    unittest.main()
