#!/bin/sh

echo "************************************************************************"
echo -e "SETUP\t Setting up ATLAS software and python3.9"
echo ""

shopt -s expand_aliases
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# Call setupATLAS.
echo "************************************************************************"
echo -e "SETUP\t Executing \"setupATLAS --quiet\""
setupATLAS --quiet || { echo -e "\"setupATLAS\" failed. Aborting ..."; return 1; }
echo ""

# Now setup a good version of python.
echo "************************************************************************"
echo -e "SETUP\t Executing \"lsetup \"python 3.9.18-x86_64-centos7\" \""
lsetup "python 3.9.18-x86_64-centos7" || { echo -e "\"lsetup \"python 3.9.18-x86_64-centos7\" \" failed. Aborting ..."; return 1; }
echo ""

# Now build the python virtual environment.
echo "************************************************************************"
echo -e "SETUP\t Constructing a working python3.9 virtual environment"
echo -e "SETUP\t Replacing python path in .venv/setupENV.sh and .venv/installPackages.sh with current python3 path:"
echo ""

which python3
sed -i "1s+^.*+pyInstall=\"$(which python3)\"+" .venv/setupENV.sh
sed -i "1s+^.*+pyInstall=\"$(which python3)\"+" .venv/installPackages.sh

# Now build the python virtual environment.
echo "************************************************************************"
echo -e "SETUP\t Building venv"
echo ""

cd .venv/
source setupENV.sh || { echo -e "\"setupENV failed\" failed. Aborting ..."; return 1; }

# Now download packages
echo "************************************************************************"
echo -e "SETUP\t Downloading the required modules, this could take some time"
echo -e "SETUP\t Keep watch for module failures"
echo ""

source installPackages.sh || { echo -e "\"setupENV failed\" failed. Aborting ..."; return 1; }
cd ../

# Now activate the python virtual environment
echo "************************************************************************"
echo -e "SETUP\t Activiating python virtual env. Executing \"source .venv/Py3p9_WorkingNu2Flows_ENV/bin/activate\""

source .venv/Py3p9_WorkingNu2Flows_ENV/bin/activate || { echo -e "sourcing venv failed. Aborting ..."; return 1; }


# Check the packages are all there and up-to-date
echo "************************************************************************"
echo -e "SETUP\t Running unit test to check modules meet requirements"
echo ""

cd tests/
python3 test_requirements.py
cd ../

echo "************************************************************************"
echo -e "SETUP\t Configuration finished!"
echo -e "SETUP\t "

