#!/bin/sh

echo "************************************************************************"
echo -e "SETUP\t Setting up ATLAS software and python3.9 environment"
echo ""

shopt -s expand_aliases
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

# Call setupATLAS.
echo "************************************************************************"
echo -e "SETUP\t Executing \"setupATLAS --quiet\""
setupATLAS --quiet || { echo -e "\"setupATLAS\" failed. Aborting ..."; return 1; }
echo ""

# Now setup a good version of python.
echo "************************************************************************"
echo -e "SETUP\t Executing \"lsetup \"python 3.9.18-x86_64-centos7\" \""
lsetup "python 3.9.18-x86_64-centos7" || { echo -e "\"lsetup \"python 3.9.18-x86_64-centos7\" \" failed. Aborting ..."; return 1; }
echo ""


# Now activate the python virtual environment.
echo "************************************************************************"
echo -e "SETUP\t Activiating python virtual env. Executing \"source .venv/Py3p9_WorkingNuFlows_ENV/bin/activate\""
source .venv/Py3p9_WorkingNu2Flows_ENV/bin/activate || { echo -e "sourcing venv failed. Aborting ..."; return 1; }

echo "************************************************************************"
echo -e "SETUP\t Configuration finished!"
echo -e "SETUP\t "

