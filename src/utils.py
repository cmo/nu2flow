from copy import deepcopy

import pyrootutils

root = pyrootutils.setup_root(search_from=__file__, pythonpath=True)

from pathlib import Path

import h5py
import numpy as np
import numpy.lib.recfunctions as rf
from dotmap import DotMap

from src.datamodules.physics import Mom4Vec, delR


def read_dilepton_file(file_path: Path, require_tops: bool = False, h5label: str = 'atlas') -> DotMap:
    """Reads in data from an HDF file, returning the collection of information
    as a DotMap object."""

    # Read the delphes table as a dotmap object
    file_data = DotMap()
    with h5py.File(file_path, "r") as f:
        table = f[h5label]
        for key in table.keys():
            try:
                file_data[key] = rf.structured_to_unstructured(table[key][:])
            except Exception:
                file_data[key] = table[key][:]

            # Neutrinos has superfluous PDGID at the front which must be removed
            # They also don't have energy!
            if key == "neutrinos":
                file_data[key] = file_data[key][..., 1:4]

            # Leptons need to be ordered particle/antiparticle just like neutrino
            if key == "leptons":
                order = np.argsort(file_data[key][..., -2])  # orders by charge
                order = np.expand_dims(order, -1)
                file_data[key] = np.take_along_axis(file_data[key], order, axis=1)

    # Change the particle entries to 4 vector objects
    fourth_mass_dict = {"MET":False, "neutrinos":True, "leptons":False, "jets":False, "truth_leptons":False, "truth_Ws":False}
    for key in ["MET", "neutrinos", "leptons", "jets", "truth_leptons", "truth_Ws"]:
        if key in list(file_data.keys()):
            file_data[key] = Mom4Vec(file_data[key], is_cartesian=False, final_is_mass=fourth_mass_dict[key])
            file_data[key].to_cartesian()

    return file_data


def argmin_last_N_axes(A, N):
    s = A.shape
    new_shp = s[:-N] + (np.prod(s[-N:]),)
    max_idx = A.reshape(new_shp).argmin(-1)
    return np.array(np.unravel_index(max_idx, s[-N:])).transpose()


def get_lj_pairing(leptons: Mom4Vec, jets: Mom4Vec, is_b: Mom4Vec) -> np.ndarray:
    # Calculate all possible lj delta R values
    leptons = deepcopy(leptons)
    jets = deepcopy(jets)
    leptons.mom = np.expand_dims(leptons.mom, 1)
    jets.mom = np.expand_dims(jets.mom, 2)
    del_r = delR(leptons, jets)

    # Make it such that if a jet is not b_tagged it receives a large distance
    del_r = np.squeeze(np.where(np.expand_dims(is_b, 2), del_r, 99999))

    # Get the minimum of the matrix
    min_1 = argmin_last_N_axes(del_r, 2)

    # Mask the matrix such that the same minimum can't be chosen again
    idx = np.arange(len(del_r))
    del_r[idx, min_1[:, 0]] = 99999
    del_r[idx, :, min_1[:, 1]] = 99999

    # Get the minimum again
    min_2 = argmin_last_N_axes(del_r, 2)

    # Typically we want the idx of each jet per input lepton
    lep_jet = np.where(min_1[:, 1] == 0, min_1[:, 0], min_2[:, 1])
    antilep_jet = np.where(min_1[:, 1] == 1, min_1[:, 0], min_2[:, 1])

    # Return them both
    return lep_jet, antilep_jet
