# ATLAS Nu2Flows
![](./network.png)

## Fork<sup>2</sup> of [Nu2Flows repository](https://github.com/rodem-hep/nu2flows/tree/master) for interface with ATLAS software

This repository contains the software and instructions required to the produce and evaluate conditional normalising flows for neutrino regression in Higgs→WW* events with $\geq1$ neutrinos. Additional utilities facilitate packaging and interfacing the inference model with existing ATLAS analysis software.

- Associated papers: https://arxiv.org/abs/2207.00664
- Associated papers: https://arxiv.org/abs/2307.02405

#### [This project has been tested and verified with python 3.9.18]

## Setup

1) Two automated setup scripts are included to set up a working environment (provided you have access to cvmfs and the necessary permissions for ATLAS software)

- For the initial setup (only needs to be done once):
```bash
                     source setup_initial.sh
```
- This will set up the ATLAS software stack (`setupATLAS`), pick an up-to-date python3 version from cvmfs, create and activate a python3 virtual environment and then download all required python modules 
  - This step could take a bit of time and needs to be monitored in case issues appear while downloading/installing specific modules. Between 3GB and 4GB of space if also required as some packages are fairly large, in particular the collection of nvidia modules are ~2GB on their own 
- Lastly a test script is run from  [tests/](tests) which validates the currently installed python3 modules against the required packages in [versions.txt](tests/versions.txt)

- Due to some urllib3 and OpenSSL version incompatibility it might also be required to run:
```bash
           pip3 install --force-reinstall -v urllib3==1.26.16
```

- Any subsequent setup after the initial time should use the basic setup script:
```bash
                          source setup.sh
```
- This simply sets up ATLAS software chooses the python3 version and activates the virtual environment created in the initial setup

- Alternatively the docker build file can be used to create an image which can run the Nu2Flows package but without any access to ATLAS-specific software.

  

## Nu2Flows Configuration

  
- This package uses [Hydra](https://hydra.cc/) and [OmegaConf](https://github.com/omry/omegaconf) for configuration.

- The main config file is [train.yaml](configs/train.yaml) which composes all others to generate a single run config
  - In this file you can specify which individual configs to use, and the random seed, etc, as well as the checkpoint used for resuming training

- More specific settings are found in one of the other config folders:

[paths](configs/paths/default.yaml):

- This is the main config file to modify. Use it to define the paths to the training and testing data `(data_dir)` as well as which files are used for training and testing. Additionally this is where the desired save directory for the model output is defined and where the project and current model name are set.

[datamodule](configs/datamodule/default.yaml):

- Defines which object kinematics to include in the training

[trainer](configs/trainer/default.yaml):

- Configuration for the PyTorch Lightning `Trainer` class with settings for the minimum and maximum training epochs, the number of GPUs to use, and the frequency of value checking.

[loggers](configs/loggers/default.yaml):

- Configures automatic logging and visualisation of the model as it runs
- The automatic logging uses [Weights and Biases](https://wandb.ai/), you will need to make a free (personal) account here: [https://wandb.ai/](https://wandb.ai/) 
- Once set up **change the entity field to match <u>your</u> wandb username**

[callbacks](configs/callbacks/default.yaml):

- Provides a collection of `lightning.pytorch.callbacks` to run during training. Keeps track of the current best performing settings, creates model checkpoints and controls settings for early stopping.

[model](configs/model/):

- Configures the model architecture and hyperparameters for training. There are three config files, one for each of the available model components. 
- By default the model is made up of a [transformer](configs/model/transformer.yaml) to produce a multiplicity-invariant event representation which is then fed into the [normalising flow](configs/model/deepset.yaml) deepset network.
  
[hydra](configs/hydra/default.yaml):

- Configures the hydra package for running, does not need to be changed.


## Running

3 executable scripts are provided

1) [train.py](scripts/train.py)

- Compiles the run config as described above and trains the model.

- Will periodically save checkpoints based on the `paths.output_dir` key.

  
2) [export.py](scripts/export.py)

- Finds the most recent best model checkpoint and runs this over the testing dataset to create an output `.h5` file containing neutrino candidates for each event in the test set.

3) [extract_from_prob.py](scripts/extract_from_prob.py)

- Extracts samples neutrinos for each event based on the target probability density and writes predictions to new file. Options are: 
  - `mode` - Pick the sample with the highest PDF value (most likely)
  - `mean` - Pick the sample closest to the mean of the PDF
  - `width` - Apply a selection to remove events with target probability distribution with a high standard deviation.
  - `mode and width` - Select events by width and then take the mode of those

## Plotting

4 example plotting scripts are provided:

1) [plot.py](scripts/plot.py)

- Loads the test set data and a model's exported neutrinos.
  - Paths are set by the user at the top of the file

- More can be done here but by default simply plots the model and truth neutrino pz component as well as the reconstructed W+ mass and pT.

2) [2Dplot.py](scripts/2Dplot.py)

- Plots the same variables but as 2D truthVreco comparisons. Can be used to assess correlation and model bias 

3) [LLplot.py](scripts/LLplot.py)

- Plots learned probability density either in kinematic regions event-by-event, or plotting the PDF values over the whole datasest.

4) [truth_reco_plot.py](scripts/truth_reco_plot.py)

- Script for producing debug plots comparing truth to reco for leptons, MET, and Ws