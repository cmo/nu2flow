"""
Create 2D truth vs reco plots
"""

import pyrootutils
root = pyrootutils.setup_root(search_from=__file__, pythonpath=True)

from pathlib import Path
import h5py
import numpy as np
from math import pi
from dotmap import DotMap
from utils.plotting import plot_corr_heatmaps
from src.datamodules.physics import Mom4Vec
from src.utils import read_dilepton_file
import sys

# Paths to the relevant files
data_file_PP8 = root / "data/Hww_dilepton/PP8_HWW_Example_test.h5"
model_file_PP8 =  root / "FlowsOutput/HWW_Example/PP8_HWW_Small/outputs/PP8_HWW_Example_model_prediction.h5"
model_file_PP8_mode = root / "FlowsOutput/HWW_Example/PP8_HWW_Small/outputs/PP8_HWW_Example_model_prediction_mode.h5"

plot_dir = root / "plots/plots2D"

# Load the event data from the file
file_data_PP8 = read_dilepton_file(Path(data_file_PP8))

print("Reading data")

# For each sample define the model neutrino as a dict and load the data
nuflow_PP8 = DotMap(
    {
        "name": "nu2flows (PP8)",
        "label": r"$\nu^2$-Flows (HWW${}_\text{ggF}$ PowhegPythia8)",
        "hist_kwargs": {"color": "b"},
    }
)
nuflow_PP8_mode = DotMap(
    {
        "name": "nu2flows_mode (PP8)",
        "label": r"$\nu^2$-Flows${}_{\text{mode}}$ (HWW${}_\text{ggF}$ PowhegPythia8)",
        "hist_kwargs": {"color": "green"},
    }
)

#parse out the data and make the kinematics into 4-vectors
with h5py.File(model_file_PP8, "r") as f:
    nuflow_PP8.log_prob = f["log_prob"][:][:] #So we can get the mode
    nuflow_PP8.gen_nu = f["gen_nu"][:] #So we can pick the right sample
    data = f["gen_nu"][:, :1]
    print(data.shape)
    nuflow_PP8.nu = Mom4Vec(data[:, :, 0])
    nuflow_PP8.anti_nu = Mom4Vec(data[:, :, 1])


with h5py.File(model_file_PP8_mode, "r") as f:
    data = f["gen_nu"][:, :1]
    mask = (data[:,:,0,0] != -999).squeeze() #Derive potential width cut mask
    data = data[mask]
    nuflow_PP8_mode.nu = Mom4Vec(data[:, :, 0])
    nuflow_PP8_mode.anti_nu = Mom4Vec(data[:, :, 1])

# For samples with truth info define the truth neutrino as a dict and load from the file
nutruth_PP8 = DotMap(
    {
        "name": "truth_nu (PP8)",
        "label": r"$\nu$ -Truth (PowhegPythia8)",
        "nu": Mom4Vec(file_data_PP8.neutrinos.mom[:, None, 0, :][mask]),
        "anti_nu": Mom4Vec(file_data_PP8.neutrinos.mom[:, None, 1, :][mask]),
        "hist_kwargs": {"color": "blue", "fill": True, "alpha": 0.5},
        "err_kwargs": {"color": "blue", "hatch": "///"},
    }
)

#For each sample get the reco lepton info
lepreco_PP8 = DotMap(
    {
        "name": "reco_lep (PP8)",
        "label": r"$\ell$-Reco (PowhegPythia8)",
        "lep": Mom4Vec(file_data_PP8.leptons.mom[:, None, 0, :][mask]),
        "anti_lep": Mom4Vec(file_data_PP8.leptons.mom[:, None, 1, :][mask]),
    }
)


#Build the Ws and Higgsies
for n in [nutruth_PP8,nuflow_PP8,nuflow_PP8_mode]:
    n.Wm = n.anti_nu + lepreco_PP8.lep
    n.Wp = n.nu + lepreco_PP8.anti_lep
    n.Higgs = n.Wm + n.Wp


# Create the plotting folder
plot_dir.mkdir(parents=True, exist_ok=True)

print("Making plots")

#Plot the 2D neutrino px, py, pz
plot_corr_heatmaps(
    x_vals = nutruth_PP8.nu.px,
    y_vals = nuflow_PP8.nu.px,
    bins = np.linspace(-90, 90, 25),
    xlabel = "px (truth-level) [GeV]",
    ylabel = "px (reconstructed-level [sample]) [GeV]",
    path = plot_dir / "TruthReco2D_nu_px.png",
    cmap = "coolwarm",
    incl_line = True,
    incl_cbar = True,
)
plot_corr_heatmaps(
    x_vals = nutruth_PP8.nu.py,
    y_vals = nuflow_PP8.nu.py,
    bins = np.linspace(-90, 90, 25),
    xlabel = "py (truth-level) [GeV]",
    ylabel = "py (reconstructed-level [sample]) [GeV]",
    path = plot_dir / "TruthReco2D_nu_py.png",
    cmap = "coolwarm",
    incl_line = True,
    incl_cbar = True,
)
plot_corr_heatmaps(
    x_vals = nutruth_PP8.nu.pz,
    y_vals = nuflow_PP8.nu.pz,
    bins = np.linspace(-140, 140, 25),
    xlabel = "pz (truth-level) [GeV]",
    ylabel = "pz (reconstructed-level [sample]) [GeV]",
    path = plot_dir / "TruthReco2D_nu_pz.png",
    cmap = "coolwarm",
    incl_line = True,
    incl_cbar = True,
)

#Plot the 2D W and H properties
plot_corr_heatmaps(
    x_vals = nutruth_PP8.Wp.pt,
    y_vals = nuflow_PP8.Wp.pt,
    bins = np.linspace(0, 200, 25),
    xlabel = "W+ boson pT (truth-level) [GeV]",
    ylabel = "W+ boson pT (reconstructed-level [sample]) [GeV]",
    path = plot_dir / "TruthReco2D_Wp_pt.png",
    cmap = "coolwarm",
    incl_line = True,
    incl_cbar = True,
    incl_vals = False,
)

####### MODE
plot_corr_heatmaps(
    x_vals = nutruth_PP8.nu.px,
    y_vals = nuflow_PP8_mode.nu.px,
    bins = np.linspace(-90, 90, 25),
    xlabel = "px (truth-level) [GeV]",
    ylabel = "px (reconstructed-level [mode]) [GeV]",
    path = plot_dir / "TruthReco2D_nu_px_mode.png",
    cmap = "coolwarm",
    incl_line = True,
    incl_cbar = True,
)
