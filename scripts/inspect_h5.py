import h5py
f_train = "../data/Hww_dilepton/PP8_HWW_Example_train.h5"
f = h5py.File(f_train, 'r')
dset = f['atlas']

data = {}
nprint = 5

for row in dset:
    data[row] = dset[row]


print("Having a look at the training data")
for key,vals in data.items():
    print("Key is: "+key)
    print("Data labels are:")
    print(vals.dtype.names)
    for i in range(nprint):
        print(vals[i])

    print("etc...")
    print("")
    
