"""
Create debugging plots comparing truth, reco, and flows kinematics for many variables
"""

import pyrootutils
root = pyrootutils.setup_root(search_from=__file__, pythonpath=True)

from pathlib import Path
import h5py
import numpy as np
import numpy.lib.recfunctions as rf
from math import pi
from dotmap import DotMap
from utils.plotting import plot_multi_hists_2
from src.datamodules.physics import Mom4Vec
from src.utils import read_dilepton_file
import sys

# Paths to the relevant files
truth_reco_file_PP8 = root / "data/Hww_dilepton/PP8_debug_test.h5" #Has lepton and W truth values 
model_file_PP8 = root / "FlowsOutput/HWW_Example/PP8_HWW_Small/outputs/PP8_HWW_Example_model_prediction.h5"

plot_dir = root / "plots/TruthRecoPlots"

# Load the truth and reco event data from the testing file
truth_reco_data_PP8 = read_dilepton_file(Path(truth_reco_file_PP8))

truth = DotMap(
    {
        "name": "truth",
        "label": r"Truth",
        "hist_kwargs": {"color": "grey", "fill": True, "alpha": 0.5},
        "err_kwargs": {"color": "grey", "hatch": "///"},
        "nu": Mom4Vec(truth_reco_data_PP8.neutrinos.mom[:, None, 0, :]),
        "anti_nu": Mom4Vec(truth_reco_data_PP8.neutrinos.mom[:, None, 1, :]),
        "comb_nu": Mom4Vec(truth_reco_data_PP8.neutrinos.mom[:, None, 0, :]) + Mom4Vec(truth_reco_data_PP8.neutrinos.mom[:, None, 1, :]),

        "lep": Mom4Vec(truth_reco_data_PP8.truth_leptons.mom[:, None, 0, :]),
        "anti_lep": Mom4Vec(truth_reco_data_PP8.truth_leptons.mom[:, None, 1, :]),
        "Wm": Mom4Vec(truth_reco_data_PP8.truth_Ws.mom[:, None, 0, :]),
        "Wp": Mom4Vec(truth_reco_data_PP8.truth_Ws.mom[:, None, 1, :]),
    }
)

truth.MET = np.sign(truth.comb_nu.energy) * np.sqrt((truth.comb_nu.energy*truth.comb_nu.energy*truth.comb_nu.pt*truth.comb_nu.pt)/(truth.comb_nu.p3_mag*truth.comb_nu.p3_mag))
truth.MPT = truth.comb_nu.pt
truth.MET_phi = truth.comb_nu.phi
truth.Higgs = truth.Wp + truth.Wm
truth.Hpt = truth.Higgs.pt

reco = DotMap(
    {
        "name": "reco",
        "label": r"Reco",
        "hist_kwargs": {"color": "r", "fill": False},
        "err_kwargs": {"color": "r", "hatch": "///", "alpha":0.4},
        "lep": Mom4Vec(truth_reco_data_PP8.leptons.mom[:, None, 0, :]),
        "anti_lep": Mom4Vec(truth_reco_data_PP8.leptons.mom[:, None, 1, :]),
    }
)

with h5py.File(truth_reco_file_PP8, "r") as f:
    data = rf.structured_to_unstructured(f["atlas"]["MET"][:])
    reco.MET = data[:,None,None,0]/1000.0
    reco.MPT = data[:,None,None,0]/1000.0
    reco.MET_phi = data[:,None,None,1]
reco.METx = reco.MET*np.sin(reco.MET_phi)
reco.METy = reco.MET*np.cos(reco.MET_phi)
reco.METvec = np.append(reco.METx+reco.lep.px+reco.anti_lep.px,reco.METy+reco.lep.py+reco.anti_lep.py,axis=1)
reco.Hpt = np.linalg.norm(reco.METvec, axis=1, keepdims=False)


# Define the model dict and load the data
flow = DotMap(
    {
        "name": "nu2flows",
        "label": r"$\nu^2$-Flows (Reco)",
        "hist_kwargs": {"color": "b"},
        "err_kwargs": {"color": "b", "hatch": "///","alpha": 0},
    }
)

with h5py.File(model_file_PP8, "r") as f:
    data = f["gen_nu"][:, :1]
    flow.nu = Mom4Vec(data[:, :, 0])
    flow.anti_nu = Mom4Vec(data[:, :, 1])

flow.comb_nu = flow.nu + flow.anti_nu
flow.MET = np.sign(flow.comb_nu.energy) * np.sqrt((flow.comb_nu.energy*flow.comb_nu.energy*flow.comb_nu.pt*flow.comb_nu.pt)/(flow.comb_nu.p3_mag*flow.comb_nu.p3_mag))
flow.MPT = flow.comb_nu.pt
flow.MET_phi = flow.comb_nu.phi
flow.Wp = flow.nu + reco.anti_lep
flow.Wm = flow.anti_nu + reco.lep
flow.Higgs = flow.Wp + flow.Wm
flow.Hpt = flow.Higgs.pt

# Define the model dict and load the data
flow_truth = DotMap(
    {
        "name": "nu2flows_truth",
        "label": r"$\nu^2$-Flows (Truth)",
        "hist_kwargs": {"color": "black"},
        "err_kwargs": {"color": "black", "hatch": "///","alpha": 0},
    }
)
flow_truth.Wp = flow.nu + truth.anti_lep
flow_truth.Wm = flow.anti_nu + truth.lep
flow_truth.Higgs = flow_truth.Wp + flow_truth.Wm
flow_truth.Hpt = flow_truth.Higgs.pt

# Combine the solutions which work for all 3
truth_reco_flows_list = [truth,reco,flow]
# Combine the solutions which only work for truth and flows
truth_flows_list = [truth,flow,flow_truth]
# Combine the solutions which only work for truth and reco
truth_reco_list = [truth,reco]

# Create the plotting folder
plot_dir.mkdir(parents=True, exist_ok=True)

# Plots with all 3 lines:
# MET (pTmiss)
plot_multi_hists_2(
    data_list=[X.MPT for X in truth_reco_flows_list],
    data_labels=[X.label for X in truth_reco_flows_list],
    col_labels="pTmiss [GeV]",
    path=plot_dir / "truth_flows_pTmiss.png",
    bins=np.linspace(0, 100, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_reco_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)
#ETmiss
plot_multi_hists_2(
    data_list=[X.MET for X in truth_reco_flows_list],
    data_labels=[X.label for X in truth_reco_flows_list],
    col_labels="ETmiss [GeV]",
    path=plot_dir / "truth_reco_flows_ETmiss.png",
    bins=np.linspace(0, 150, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_reco_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

# MET_phi
plot_multi_hists_2(
    data_list=[X.MET_phi for X in truth_reco_flows_list],
    data_labels=[X.label for X in truth_reco_flows_list],
    col_labels="ETmiss $\phi$ [rad]",
    path=plot_dir / "truth_reco_flows_METPhi.png",
    bins=np.linspace(-pi, pi, 25),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_reco_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

# Higgs pT
plot_multi_hists_2(
    data_list=[X.Hpt for X in truth_reco_flows_list+[flow_truth]],
    data_labels=[X.label for X in truth_reco_flows_list+[flow_truth]],
    col_labels="Higgs pT [GeV]",
    path=plot_dir / "truth_reco_flows_HpT.png",
    bins=np.linspace(0, 200, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_flows_list+[flow_truth]],
    err_kwargs=[X.err_kwargs for X in truth_reco_flows_list+[flow_truth]],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

# Plots with truth and reco:
# lepton px
plot_multi_hists_2(
    data_list=[X.lep.px for X in truth_reco_list],
    data_labels=[X.label for X in truth_reco_list],
    col_labels="Lepton px [GeV]",
    path=plot_dir / "truth_reco_lepton_px.png",
    bins=np.linspace(-100, 100, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_list],
    err_kwargs=[X.err_kwargs for X in truth_reco_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)
# lepton py
plot_multi_hists_2(
    data_list=[X.lep.py for X in truth_reco_list],
    data_labels=[X.label for X in truth_reco_list],
    col_labels="Lepton py [GeV]",
    path=plot_dir / "truth_reco_lepton_py.png",
    bins=np.linspace(-100, 100, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_list],
    err_kwargs=[X.err_kwargs for X in truth_reco_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)
# lepton pz
plot_multi_hists_2(
    data_list=[X.lep.pz for X in truth_reco_list],
    data_labels=[X.label for X in truth_reco_list],
    col_labels="Lepton pz [GeV]",
    path=plot_dir / "truth_reco_lepton_pz.png",
    bins=np.linspace(-100, 100, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_list],
    err_kwargs=[X.err_kwargs for X in truth_reco_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)
# lepton pt
plot_multi_hists_2(
    data_list=[X.lep.pt for X in truth_reco_list],
    data_labels=[X.label for X in truth_reco_list],
    col_labels="Lepton pT [GeV]",
    path=plot_dir / "truth_reco_lepton_pt.png",
    bins=np.linspace(20, 150, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_list],
    err_kwargs=[X.err_kwargs for X in truth_reco_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)
# lepton energy
plot_multi_hists_2(
    data_list=[X.lep.energy for X in truth_reco_list],
    data_labels=[X.label for X in truth_reco_list],
    col_labels="Lepton energy [GeV]",
    path=plot_dir / "truth_reco_lepton_E.png",
    bins=np.linspace(0, 200, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_list],
    err_kwargs=[X.err_kwargs for X in truth_reco_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)
# lepton eta
plot_multi_hists_2(
    data_list=[X.lep.eta for X in truth_reco_list],
    data_labels=[X.label for X in truth_reco_list],
    col_labels="Lepton pseudorapidity",
    path=plot_dir / "truth_reco_lepton_eta.png",
    bins=np.linspace(-3, 3, 25),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_list],
    err_kwargs=[X.err_kwargs for X in truth_reco_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)
# lepton phi
plot_multi_hists_2(
    data_list=[X.lep.phi for X in truth_reco_list],
    data_labels=[X.label for X in truth_reco_list],
    col_labels="Lepton $\phi$ [Rad]",
    path=plot_dir / "truth_reco_lepton_phi.png",
    bins=np.linspace(-pi, pi, 25),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_reco_list],
    err_kwargs=[X.err_kwargs for X in truth_reco_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

# Plots with truth and flows:

#W+ px
plot_multi_hists_2(
    data_list=[X.Wp.px for X in truth_flows_list],
    data_labels=[X.label for X in truth_flows_list],
    col_labels="W+ px [GeV]",
    path=plot_dir / "truth_flows_Wp_px.png",
    bins=np.linspace(-100, 100, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

#W+ py
plot_multi_hists_2(
    data_list=[X.Wp.py for X in truth_flows_list],
    data_labels=[X.label for X in truth_flows_list],
    col_labels="W+ py [GeV]",
    path=plot_dir / "truth_flows_Wp_py.png",
    bins=np.linspace(-100, 100, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

#W+ pz
plot_multi_hists_2(
    data_list=[X.Wp.pz for X in truth_flows_list],
    data_labels=[X.label for X in truth_flows_list],
    col_labels="W+ pz [GeV]",
    path=plot_dir / "truth_flows_Wp_pz.png",
    bins=np.linspace(-150, 150, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

#W+ pT
plot_multi_hists_2(
    data_list=[X.Wp.pt for X in truth_flows_list],
    data_labels=[X.label for X in truth_flows_list],
    col_labels="W+ pT [GeV]",
    path=plot_dir / "truth_flows_Wp_pT.png",
    bins=np.linspace(0, 200, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

#W+ mass
plot_multi_hists_2(
    data_list=[X.Wp.m for X in truth_flows_list],
    data_labels=[X.label for X in truth_flows_list],
    col_labels="W+ mass [GeV]",
    path=plot_dir / "truth_flows_Wp_m.png",
    bins=np.linspace(0, 100, 50),
    do_err=True,
    do_norm=True,
    hist_kwargs=[X.hist_kwargs for X in truth_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

#W+ energy
plot_multi_hists_2(
    data_list=[X.Wp.energy for X in truth_flows_list],
    data_labels=[X.label for X in truth_flows_list],
    col_labels="W+ energy [GeV]",
    path=plot_dir / "truth_flows_Wp_E.png",
    bins=np.linspace(20, 200, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

#W- pT
plot_multi_hists_2(
    data_list=[X.Wm.pt for X in truth_flows_list],
    data_labels=[X.label for X in truth_flows_list],
    col_labels="W- pT [GeV]",
    path=plot_dir / "truth_flows_Wm_pT.png",
    bins=np.linspace(0, 200, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

#W- mass
plot_multi_hists_2(
    data_list=[X.Wm.m for X in truth_flows_list],
    data_labels=[X.label for X in truth_flows_list],
    col_labels="W- mass [GeV]",
    path=plot_dir / "truth_flows_Wm_m.png",
    bins=np.linspace(0, 100, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

#W- energy
plot_multi_hists_2(
    data_list=[X.Wm.energy for X in truth_flows_list],
    data_labels=[X.label for X in truth_flows_list],
    col_labels="W- energy [GeV]",
    path=plot_dir / "truth_flows_Wm_E.png",
    bins=np.linspace(20, 200, 50),
    do_err=True,
    hist_kwargs=[X.hist_kwargs for X in truth_flows_list],
    err_kwargs=[X.err_kwargs for X in truth_flows_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)
