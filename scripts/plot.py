"""
Create a simple plot comparing the test set neutrino kinematics to truth
"""

import pyrootutils
root = pyrootutils.setup_root(search_from=__file__, pythonpath=True)

from pathlib import Path
import h5py
import numpy as np
from math import pi
from dotmap import DotMap
from utils.plotting import plot_multi_hists_2
from src.datamodules.physics import Mom4Vec
from src.utils import read_dilepton_file
import sys

debug_plots = False
# Paths to the relevant files
data_file_PP8 = root / "data/Hww_dilepton/PP8_HWW_Example_test.h5"
model_file_PP8 =  root / "FlowsOutput/HWW_Example/PP8_HWW_Small/outputs/PP8_HWW_Example_model_prediction.h5"
model_file_PP8_mode = root / "FlowsOutput/HWW_Example/PP8_HWW_Small/outputs/PP8_HWW_Example_model_prediction_mode.h5"

plot_dir = root / "plots/plots1D"

# Load the event data from the file
file_data = read_dilepton_file(Path(data_file_PP8))

# Define the model neutrino as a dict and load the data
nuflow_PP8 = DotMap(
    {
        "name": "nu2flows",
        "label": r"$\nu^2$-Flows",
        "hist_kwargs": {"color": "b"},
    }
)

nuflow_PP8_mode = DotMap(
    {
        "name": "nu2flows_mode (PP8)",
        "label": r"$\nu^2$-Flows${}_{\text{mode}}$ (HWW${}_\text{ggF}$ PowhegPythia8)",
        "hist_kwargs": {"color": "green"},
    }
)

with h5py.File(model_file_PP8_mode, "r") as f:
    data = f["gen_nu"][:, :1]
    mask = (data[:,:,0,0] != -999).squeeze() #Derive potential width cut mask
    data = data[mask]
    nuflow_PP8_mode.nu = Mom4Vec(data[:, :, 0])
    nuflow_PP8_mode.anti_nu = Mom4Vec(data[:, :, 1])
with h5py.File(model_file_PP8, "r") as f:
    data = f["gen_nu"][:, :1][mask]
    nuflow_PP8.nu = Mom4Vec(data[:, :, 0])
    nuflow_PP8.anti_nu = Mom4Vec(data[:, :, 1])


# Define the truth neutrino as a dict and load from the file
nutruth_PP8 = DotMap(
    {
        "name": "truth_nu",
        "label": r"$\nu$-Truth",
        "nu": Mom4Vec(file_data.neutrinos.mom[:, None, 0, :][mask]),
        "anti_nu": Mom4Vec(file_data.neutrinos.mom[:, None, 1, :][mask]),
        "hist_kwargs": {"color": "grey", "fill": True, "alpha": 0.5},
        "err_kwargs": {"color": "grey", "hatch": "///"},
    }
)

leptruth_PP8 = DotMap(
    {
        "name": "truth_lep",
        "label": r"$\ell$-Truth",
        "lep": Mom4Vec(file_data.leptons.mom[:, None, 0, :][mask]),
        "anti_lep": Mom4Vec(file_data.leptons.mom[:, None, 1, :][mask]),
    }
)


# Combine the two neutrino types into a single list
neutrino_list = [nutruth_PP8,nuflow_PP8,nuflow_PP8_mode]

# Create the plotting folder
plot_dir.mkdir(parents=True, exist_ok=True)

# Plot the neutrino pZ
plot_multi_hists_2(
    data_list=[n.anti_nu.pz for n in neutrino_list],
    data_labels=[n.label for n in neutrino_list],
    col_labels="$\nu$ pz [GeV]",
    path=plot_dir / "nu_pz_SampleVMode.png",
    bins=np.linspace(-150, 150, 100),
    do_err=True,
    do_norm=True,
    hist_kwargs=[n.hist_kwargs for n in neutrino_list],
    err_kwargs=[n.err_kwargs for n in neutrino_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

for n in neutrino_list:
    n.Wm = n.anti_nu + leptruth_PP8.lep
    n.Wp = n.nu + leptruth_PP8.anti_lep
    n.Higgs = n.Wm + n.Wp

# Plot the W+ mass
plot_multi_hists_2(
    data_list=[n.Wp.mass for n in neutrino_list],
    data_labels=[n.label for n in neutrino_list],
    col_labels="$W^{+}$ boson mass [GeV]",
    path=plot_dir / "Wplus_mass_SampleVMode.png",
    bins=np.linspace(0, 150, 75),
    do_err=True,
    do_norm=True,
    hist_kwargs=[n.hist_kwargs for n in neutrino_list],
    err_kwargs=[n.err_kwargs for n in neutrino_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)

# Plot the W+ pT
plot_multi_hists_2(
    data_list=[n.Wp.pt for n in neutrino_list],
    data_labels=[n.label for n in neutrino_list],
    col_labels="$W^{+}$ boson pT [GeV]",
    path=plot_dir / "Wplus_pT_SampleVMode.png",
    bins=np.linspace(0, 200, 100),
    do_err=True,
    do_norm=True,
    hist_kwargs=[n.hist_kwargs for n in neutrino_list],
    err_kwargs=[n.err_kwargs for n in neutrino_list],
    incl_overflow = False,
    incl_underflow = False,
    do_ratio_to_first = True
)
