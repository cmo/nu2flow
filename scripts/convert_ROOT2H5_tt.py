import uproot as up
import numpy as np
import awkward as ak
import h5py
import argparse

def main(inputfile: str, outputfiletrain: str, outputfiletest: str, do_neutrino: bool, do_truth_debug: bool):

    events = up.open(inputfile)['nominal']
    if do_neutrino:
        truth = up.open(inputfile)['neutrino_truth']
    else:
        truth = up.open(inputfile)['nominal']

    #truth match to event tree with eventnumber
    print("Getting truth<->reco matching")
    _,truthidx,_=np.intersect1d(truth['eventNumber'].array(library='np'),events['eventNumber'].array(library='np'),return_indices=True)
    _,recoidx,_=np.intersect1d(events['eventNumber'].array(library='np'),truth['eventNumber'].array(library='np'),return_indices=True)
    truthidx = np.sort(truthidx)
    recoidx = np.sort(recoidx)
    print("Truth good event IDs:")
    print(truthidx)
    print("Reco good event IDs")
    print(recoidx)
    
    ## get jets, convert
    print("Converting jets")

    jetvars=['jet_pt','jet_eta','jet_phi','jet_e','jet_isbtagged_DL1r_85==1']
    jets = ak.pad_none(events.arrays(jetvars,filter_name="jet_*"),10,clip=True)
    jetarr = np.ma.concatenate([np.ma.expand_dims(ak.to_numpy(jets[var]),-1) for var in jetvars],-1)
    jetarr = np.nan_to_num(jetarr)
    jetarr = jetarr[recoidx]
    
    njets = np.sum(~jetarr.mask.any(axis=-1),axis=1)
    njets = njets.astype(np.int16) #Might as well save some space
    print("Total number of small-R jets before ID check: "+str(ak.count(jets['jet_pt'])))
    print("Total number of small-R jets after ID check: "+str(njets.sum()))

    nbjets = np.sum(jetarr,axis=1,dtype=np.int16)[:,-1]
    print("Total number of b-tagged jets before ID check: "+str(ak.sum(jets['jet_isbtagged_DL1r_85==1'])))
    print("Total number of b-tagged jets after ID check: "+str(nbjets.sum()))
    
    
    print(jetarr)
    input()

    if do_neutrino:
        print("Converting neutrinos")
        nuvars1=['nu_pdgid','nu_pt','nu_eta','nu_phi','nu_m']
        nuvars2=['nubar_pdgid','nubar_pt','nubar_eta','nubar_phi','nubar_m']
        nutruth1 = truth.arrays(nuvars1,filter_name="nu_*")
        nutruth2 = truth.arrays(nuvars2,filter_name="nubar_*")
        nutrutharr1 = np.concatenate([np.ma.expand_dims(ak.to_numpy(nutruth1[var]),-1) for var in nuvars1],-1)
        nutrutharr2 = np.concatenate([np.ma.expand_dims(ak.to_numpy(nutruth2[var]),-1) for var in nuvars2],-1)

        #Make sure neutrino is always first and anti-neutrino is always second
        nutrutharr = np.empty(np.shape(nutrutharr1))
        nubtrutharr = np.empty(np.shape(nutrutharr2))
        for x,arr in enumerate(nutrutharr1):
            barcheck = nutrutharr1[x][0]
            if barcheck>0:
                nutrutharr[x] = nutrutharr1[x]
                nubtrutharr[x] = nutrutharr2[x]
            else:
                nutrutharr[x] = nutrutharr2[x]
                nubtrutharr[x] = nutrutharr1[x]

        #Based on method outlined here:
        #https://stackoverflow.com/questions/5347065/interleaving-two-numpy-arrays-efficiently
        rows = np.size(nutrutharr,0)
        cols = np.size(nutrutharr,1)
        shapesize = rows,2,cols

        neutrinos = np.empty(shape=shapesize, dtype=nutrutharr.dtype)
        neutrinos[::1, ::2] = np.reshape(nutrutharr, (rows,1,cols))
        neutrinos[::1, 1::2] = np.reshape(nubtrutharr, (rows,1,cols))

        print("shape of neutrinos before ID check",np.shape(neutrinos))
        neutrinos = neutrinos[truthidx]
        print("shape of neutrinos after ID check",np.shape(neutrinos))
        bad_tau_check = neutrinos[:,0,3] > -10

    else:
        print("Spoofing neutrino info")
        shapesize = np.size(recoidx,0),2,5
        neutrinos = np.ones(shape=shapesize, dtype=np.float64)
        PDGIDs = np.random.randint(-10,10,(np.size(recoidx,0),2,1))
        PTs = np.random.rand(np.size(recoidx,0),2,1)*100
        ETAs = np.random.rand(np.size(recoidx,0),2,1)*5
        PHIs = np.random.rand(np.size(recoidx,0),2,1)*3.14159
        Ms = np.random.rand(np.size(recoidx,0),2,1)*0
        MULT = np.c_[PDGIDs,PTs,ETAs,PHIs,Ms]
        neutrinos *= MULT

    # Get event variables
    print("Converting event level")
    eventvars=['met_met','met_phi','eventNumber']
    evarr = ak.to_numpy(events.arrays(eventvars))
    evarr = evarr[recoidx]

    METvars=['met_met','met_phi']
    METarr = np.ma.concatenate([np.ma.expand_dims(ak.to_numpy(events.arrays(METvars)[var]),-1) for var in METvars],-1)
    METarr = METarr[recoidx]

    # Get lepton variables
    print("Converting leptons")
    lepvars=['lep_pt','lep_eta','lep_phi','lep_e','lep_charge','abs(lep_pdgid) > 12']
    leps = ak.pad_none(events.arrays(lepvars,filter_name="Leptons_*"),2,clip=True)
    leparr = np.ma.concatenate([np.ma.expand_dims(ak.to_numpy(leps[var]),-1) for var in lepvars],-1)
    print("shape of leptons before ID check",np.shape(leparr))
    leparr = leparr[recoidx]
    print("shape of leptons after ID check",np.shape(leparr))

    if do_truth_debug:
        # Get truth lepton variables
        print("Converting truth leptons")
        truth_lepvars1=['truth_W1_DecayProduct1_pt','truth_W1_DecayProduct1_eta','truth_W1_DecayProduct1_phi','truth_W1_DecayProduct1_e','-1*truth_W1_DecayProduct1_pdgId/abs(truth_W1_DecayProduct1_pdgId)','abs(truth_W1_DecayProduct1_pdgId) > 12']
        truth_lepvars2=['truth_W2_DecayProduct1_pt','truth_W2_DecayProduct1_eta','truth_W2_DecayProduct1_phi','truth_W2_DecayProduct1_e','-1*truth_W2_DecayProduct1_pdgId/abs(truth_W2_DecayProduct1_pdgId)','abs(truth_W2_DecayProduct1_pdgId) > 12']
        truth_leps1 = truth.arrays(truth_lepvars1,filter_name="truth_W1_DecayProduct1_*")
        truth_leps2 = truth.arrays(truth_lepvars2,filter_name="truth_W2_DecayProduct1_*")
        truth_leparr1 = np.ma.concatenate([np.ma.expand_dims(ak.to_numpy(truth_leps1[var]),-1) for var in truth_lepvars1],-1)
        truth_leparr2 = np.ma.concatenate([np.ma.expand_dims(ak.to_numpy(truth_leps2[var]),-1) for var in truth_lepvars2],-1)
        #Cut before the loop
        print("shape of truth_leptons1 before ID check",np.shape(truth_leparr1))
        print("shape of truth_leptons2 before ID check",np.shape(truth_leparr2))
        truth_leparr1 = truth_leparr1[truthidx]
        truth_leparr2 = truth_leparr2[truthidx]
        print("shape of truth_leptons1 after ID check",np.shape(truth_leparr1))
        print("shape of truth_leptons2 after ID check",np.shape(truth_leparr2))

        # Get truth W variables
        print("Converting truth W-bosons")
        truth_Wvars1=['truth_W1_pt','truth_W1_eta','truth_W1_phi','truth_W1_e','truth_W1_DecayProduct1_pdgId/abs(truth_W1_DecayProduct1_pdgId)']
        truth_Wvars2=['truth_W2_pt','truth_W2_eta','truth_W2_phi','truth_W2_e','truth_W2_DecayProduct1_pdgId/abs(truth_W2_DecayProduct1_pdgId)']
        truth_Ws1 = truth.arrays(truth_Wvars1,filter_name="truth_W1_*")
        truth_Ws2 = truth.arrays(truth_Wvars2,filter_name="truth_W2_*")
        truth_Warr1 = np.ma.concatenate([np.ma.expand_dims(ak.to_numpy(truth_Ws1[var]),-1) for var in truth_Wvars1],-1)
        truth_Warr2 = np.ma.concatenate([np.ma.expand_dims(ak.to_numpy(truth_Ws2[var]),-1) for var in truth_Wvars2],-1)
        #Cut before the loop
        print("shape of truth_W1 before ID check",np.shape(truth_Warr1))
        print("shape of truth_W2 before ID check",np.shape(truth_Warr2))
        truth_Warr1 = truth_Warr1[truthidx]
        truth_Warr2 = truth_Warr2[truthidx]
        print("shape of truth_W1 after ID check",np.shape(truth_Warr1))
        print("shape of truth_W2 after ID check",np.shape(truth_Warr2))

        #Make sure l- is always first and l+ is always second
        #Make sure W- is always first and W+ is always second
        lepmtrutharr = np.empty(np.shape(truth_leparr1))
        lepptrutharr = np.empty(np.shape(truth_leparr1))
        Wmtrutharr = np.empty(np.shape(truth_Warr1))
        Wptrutharr = np.empty(np.shape(truth_Warr1))
        for x in range(truth_leparr1.shape[0]):
            lepcheck = truth_leparr1[x][-2]
            Wcheck = truth_Warr1[x][-1]
            if lepcheck>0:
                lepptrutharr[x] = truth_leparr1[x]
                lepmtrutharr[x] = truth_leparr2[x]
            else:
                lepptrutharr[x] = truth_leparr2[x]
                lepmtrutharr[x] = truth_leparr1[x]
            if Wcheck>0:
                Wptrutharr[x] = truth_Warr1[x]
                Wmtrutharr[x] = truth_Warr2[x]
            else:
                Wptrutharr[x] = truth_Warr2[x]
                Wmtrutharr[x] = truth_Warr1[x]

        rows = np.size(lepptrutharr,0)
        cols = np.size(lepptrutharr,1)
        shapesize = rows,2,cols
        tw_rows = np.size(Wmtrutharr,0)
        tw_cols = np.size(Wmtrutharr,1)
        tw_shapesize = tw_rows,2,tw_cols

        truth_leptons = np.empty(shape=shapesize, dtype=lepmtrutharr.dtype)
        truth_leptons[::1, ::2] = np.reshape(lepmtrutharr, (rows,1,cols))
        truth_leptons[::1, 1::2] = np.reshape(lepptrutharr, (rows,1,cols))

        truth_Ws = np.empty(shape=tw_shapesize, dtype=Wmtrutharr.dtype)
        truth_Ws[::1, ::2] = np.reshape(Wmtrutharr, (tw_rows,1,tw_cols))
        truth_Ws[::1, 1::2] = np.reshape(Wptrutharr, (tw_rows,1,tw_cols))

        print("shape of truth leptons after ID check",np.shape(truth_leptons))
        print("shape of truth Ws after ID check",np.shape(truth_Ws))


    print("Saving all")
    jetnamevars=["pt","eta","phi","energy","is_tagged"]
    METnamevars=["MET","phi"]
    lepnamevars=["pt","eta","phi","energy","charge","type"]
    #nunamevars=["PDGID","pt","eta","phi"]
    nunamevars=["PDGID","pt","eta","phi","mass"]
    truth_lepnamevars=["pt","eta","phi","energy","charge","type"]
    truth_Wnamevars=["pt","eta","phi","energy","charge"]

    hf_train = h5py.File(outputfiletrain, 'w')
    hf_test = h5py.File(outputfiletest, 'w')
    #train = evarr['eventNumber'] % 2 > 0 #50%
    #test = evarr['eventNumber'] % 2 == 0 #50%
    train = evarr['eventNumber'] % 10 > 0 #90%
    test = evarr['eventNumber'] % 10 == 0 #10%
    if do_neutrino:
        test = test*bad_tau_check
        train = train*bad_tau_check
    group = f'atlas'
    hf_train.create_group(group)
    hf_test.create_group(group)
    jets_dt = np.dtype( { 'names':jetnamevars,
                          'formats':(np.float32, np.float32, np.float32, np.float32, np.float32) } )
    jets_recarray = np.rec.array(jetarr,dtype=jets_dt).squeeze(axis=2)
    #Convert b-tagging column to bool
    jets_recarray = jets_recarray.astype([('pt', '<f4'), ('eta', '<f4'), ('phi', '<f4'), ('energy', '<f4'), ('is_tagged', '<?')])
    hf_train[group].create_dataset('jets',data=jets_recarray[train])
    hf_test[group].create_dataset('jets',data=jets_recarray[test])

    leptons_dt = np.dtype( { 'names':lepnamevars,
                             'formats':(np.float64, np.float64, np.float64, np.float64, np.float64, np.float64) } )
    leptons_recarray = np.rec.array(leparr,dtype=leptons_dt).squeeze(axis=2)
    hf_train[group].create_dataset('leptons',data=leptons_recarray[train])
    hf_test[group].create_dataset('leptons',data=leptons_recarray[test])

    MET_dt = np.dtype( { 'names':METnamevars,
                         'formats':(np.float32,np.float32) } )
    MET_recarray = np.rec.array(METarr,dtype=MET_dt).flatten()
    hf_train[group].create_dataset('MET',data=MET_recarray[train])
    hf_train[group]['njets'] = njets[train]
    hf_train[group]['nbjets'] = nbjets[train]
    hf_train[group]['eventNumber'] = evarr['eventNumber'][train]
    hf_test[group].create_dataset('MET',data=MET_recarray[test])
    hf_test[group]['njets'] = njets[test]
    hf_test[group]['nbjets'] = nbjets[test]
    hf_test[group]['eventNumber'] = evarr['eventNumber'][test]
    neutrinos_dt = np.dtype( { 'names':nunamevars,
                               'formats':(np.float64, np.float64, np.float64, np.float64, np.float64) } )
    neutrinos_recarray = np.rec.array(neutrinos,dtype=neutrinos_dt).squeeze(axis=2)
    hf_test[group].create_dataset('neutrinos',data=neutrinos_recarray[test])
    hf_train[group].create_dataset('neutrinos',data=neutrinos_recarray[train])

    if do_truth_debug:
        truth_leptons_dt = np.dtype( { 'names':truth_lepnamevars,
                                       'formats':(np.float64, np.float64, np.float64, np.float64, np.float64, np.float64) } )
        truth_leptons_recarray = np.rec.array(truth_leptons,dtype=truth_leptons_dt).squeeze(axis=2)
        hf_train[group].create_dataset('truth_leptons',data=truth_leptons_recarray[train])
        hf_test[group].create_dataset('truth_leptons',data=truth_leptons_recarray[test])

        truth_Ws_dt = np.dtype( { 'names':truth_Wnamevars,
                                 'formats':(np.float64, np.float64, np.float64, np.float64, np.float64) } )
        truth_Ws_recarray = np.rec.array(truth_Ws,dtype=truth_Ws_dt).squeeze(axis=2)
        hf_train[group].create_dataset('truth_Ws',data=truth_Ws_recarray[train])
        hf_test[group].create_dataset('truth_Ws',data=truth_Ws_recarray[test])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("inputfile", type=str, help="input ROOT file to process")
    parser.add_argument("outputfiletrain", type=str, help="output HDF5 file for training")
    parser.add_argument("outputfiletest", type=str, help="output HDF5 file for testing")
    parser.add_argument("--nu", action=argparse.BooleanOptionalAction, dest='do_neutrino', default=True, help="whether to include neutrino info")
    parser.add_argument("--debug", action=argparse.BooleanOptionalAction, dest='do_truth_debug', default=False, help="flag to turn on/off debugging truth info")

    main(**parser.parse_args().__dict__)
