"""
Extract data based on probability density and write out to new file
"""

import pyrootutils
root = pyrootutils.setup_root(search_from=__file__, pythonpath=True)

from pathlib import Path
import h5py
import numpy as np
from dotmap import DotMap
from utils.plottingHWW import extract_from_target

force_remake = True
# Paths to the relevant files
model_file_PP8 =  root / "FlowsOutput/HWW_Example/PP8_HWW_Small/outputs/PP8_HWW_Example_model_prediction.h5"
output_file = root / "FlowsOutput/HWW_Example/PP8_HWW_Small/outputs/PP8_HWW_Example_model_prediction_mode.h5"

# For each sample define the model neutrino as a dict and load the data
nuflow_PP8 = DotMap(
    {
        "name": "nu2flows (PP8)",
    }
)
with h5py.File(model_file_PP8, "r") as f:
    nuflow_PP8.log_prob = f["log_prob"][:][:] #So we can get the mode
    nuflow_PP8.gen_nu = f["gen_nu"][:] #So we can pick the right sample

if (not output_file.is_file() or force_remake):
    print("Extracting data based on probability density, writing out to: ",str(output_file))
    extract_from_target(
        log_probs = nuflow_PP8.log_prob, 
        flows = nuflow_PP8.gen_nu,
        path = output_file,
        mode = "mode", #Options are mode, mean, width, mode_and_width
        width_range = [0.0,2.5],
    )
