"""
Create a simple plot comparing sampled neutrino kinematic values from Flows PDF for test set vs truth
"""

import pyrootutils
root = pyrootutils.setup_root(search_from=__file__, pythonpath=True) #Get root dir

from pathlib import Path
import h5py
import numpy as np
from dotmap import DotMap
from math import pi

from utils.plottingHWW import plot_event_prediction_hist, plot_logprob
from src.datamodules.physics import Mom4Vec
from src.utils import read_dilepton_file
import sys

e_first = 0
e_last = 4
evt_by_evt = True
log_prob = False
# Paths to the relevant files
data_file_PP8 = root / "data/Hww_dilepton/PP8_HWW_Example_test.h5"
model_file_PP8 =  root / "FlowsOutput/HWW_Example/PP8_HWW_Small/outputs/PP8_HWW_Example_model_prediction.h5"
model_file_PP8_mode = root / "FlowsOutput/HWW_Example/PP8_HWW_Small/outputs/PP8_HWW_Example_model_prediction_mode.h5"
plot_dir = root / "plots/plotsLL"

# Load the event data from the file
file_data_PP8 = read_dilepton_file(Path(data_file_PP8))

# Define the model neutrino as a dict and load the data
nuflow = DotMap(
    {
        "name": "nu2flows",
        "nu_label": r"$\nu^2$-Flows",
        "anti_nu_label": r"$\bar{\nu}^2$-Flows",
        "nu_hist_kwargs": {"color": "blue", "linestyle": "solid", "fill": False, "alpha": 1.0},
        "anti_nu_hist_kwargs": {"color": "red", "linestyle": "solid", "fill": False, "alpha": 1.0},
        "PP8_label": r"$\nu^2$-Flows (HWW${}_\text{ggF}$ PowhegPythia8)",
        "PP8_hist_kwargs": {"color": "blue", "linestyle": "solid", "fill": False, "alpha": 1.0},
    }
)
with h5py.File(model_file_PP8, "r") as f:
    data = f["gen_nu"][e_first:e_last] #All 255 samples for the first 10 events
    nuflow.log_prob = f["log_prob"][:][:] #Combined nu and nati_nu -ve log likelihood? for each sample
    nuflow.nu = Mom4Vec(data[:, :, 0])
    nuflow.anti_nu = Mom4Vec(data[:, :, 1])

# Define the truth neutrino as a dict and load from the file
nutruth = DotMap(
    {
        "name": "truth_nu",
        "nu_label": r"$\nu$ -Truth",
        "anti_nu_label": r"$\bar{\nu}$ -Truth",
        "PP8_label": r"$\nu$ -Truth (PowhegPythia8)",
        "nu": Mom4Vec(file_data_PP8.neutrinos.mom[:, None, 0, :][e_first:e_last]),
        "anti_nu": Mom4Vec(file_data_PP8.neutrinos.mom[:, None, 1, :][e_first:e_last]),
        "nu_hist_kwargs": {"color": "blue", "linestyle": "dashed"},
        "anti_nu_hist_kwargs": {"color": "red", "linestyle": "dashed"},
    }
)


#For each sample get the reco lepton info
lepreco_PP8 = DotMap(
    {
        "name": "reco_lep (PP8)",
        "label": r"$\ell$-Reco (PowhegPythia8)",
        "lep": Mom4Vec(file_data_PP8.leptons.mom[:, None, 0, :][e_first:e_last]),
        "anti_lep": Mom4Vec(file_data_PP8.leptons.mom[:, None, 1, :][e_first:e_last]),
    }
)

# Combine the two neutrino types into a single list
neutrino_list = [nuflow, nutruth]

#Build the Ws and Higgsies
for n in neutrino_list:
    n.Wm = n.anti_nu + lepreco_PP8.lep
    n.Wp = n.nu + lepreco_PP8.anti_lep
    n.Higgs = n.Wm + n.Wp

# Create the plotting folder
plot_dir.mkdir(parents=True, exist_ok=True)

if evt_by_evt:
    
    plot_event_prediction_hist(
        start = e_first,
        end = e_last,
        flows=[nuflow.nu.pz, nuflow.anti_nu.pz],
        truth=[nutruth.nu.pz, nutruth.anti_nu.pz],
        flows_labels=[nuflow.nu_label, nuflow.anti_nu_label],
        truth_labels=[nutruth.nu_label, nutruth.anti_nu_label],
        x_label="Neutrino pz",
        y_label="Arbitrary units",
        path=plot_dir / "CombinedNu_pz_PP8_HWW_evt.png",
        bins=np.linspace(-150, 150, 50),
        flows_kwargs=[nuflow.nu_hist_kwargs,nuflow.anti_nu_hist_kwargs],
        truth_kwargs=[nutruth.nu_hist_kwargs,nutruth.anti_nu_hist_kwargs],
        incl_overflow = False,
        incl_underflow = False,
        incl_flows_pred = False,
    )
    

if log_prob:

    plot_logprob(
        log_probs=[nuflow.log_prob],
        log_probs_labels=[nuflow.PP8_label],
        x_label="Standard deviation log probability density",
        y_label="Normalised Nevents",
        path=plot_dir / "LogProb_PP8_HWW_Std.png",
        bins=np.linspace(1, 3.5, 25),
        log_probs_kwargs=[nuflow.PP8_hist_kwargs],
        incl_overflow=False,
        incl_underflow=False,
        incl_all_samples=False,
        do_mean=False,
        do_std=True,
        do_norm=True,
    )
