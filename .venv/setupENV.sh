pyInstall="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/python/3.9.18-x86_64-centos7/bin/python3"
ENVname="Py3p9_WorkingNu2Flows_ENV"

${pyInstall} -m venv $(pwd)/${ENVname}
